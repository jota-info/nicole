from django.conf.urls import include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
from projeto.views import Home
from projeto.views import CadastroProfessor, CadastroOcorrencia, CadastroAluno, MostrarAlunos, PerfilAluno, PerfilProfessor, MostrarOcorrencias, OcorrenciaDetalhe, AlterOcorrencia

urlpatterns = [
	url(r'^static/(.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^login/', "django.contrib.auth.views.login", {
                "template_name": "login.html" }),
    url(r'^logout/', "django.contrib.auth.views.logout_then_login", {
            'login_url': '/login/'}),

    url(r'^$', Home.as_view(), name='home'),
    url(r'^cadprofessor/', CadastroProfessor.as_view(), name='cadprofessor'),
    url(r'^perfilProfessor/', PerfilProfessor.as_view(), name='perfilProfessor'),
    url(r'^cadOcorrencias/', CadastroOcorrencia.as_view(), name='cadOcorrencias'),
    url(r'^cadOcorrencias/', CadastroOcorrencia.as_view(), name='cadOcorrencias'),
    url(r'^ocorrencias/', MostrarOcorrencias.as_view(), name='ocorrencias'),
    url(r'^ocorrenciaDetalhe/(?P<id>\d+)/', OcorrenciaDetalhe.as_view(), name='ocorrenciaDetalhe'),
    url(r'^alterOcorrencia/(?P<id>\d+)/', AlterOcorrencia.as_view(), name='alterOcorrencia'),
    url(r'^cadAluno/', CadastroAluno.as_view(), name='cadAluno'),
    url(r'^mostrarAlunos/', MostrarAlunos.as_view(), name='mostrarAlunos'),
    url(r'^perfilAluno/(?P<id>\d+)/', PerfilAluno.as_view(), name='perfilAluno'),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)


from django.views.generic.base import View
from django.shortcuts import render
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from projeto.models.professor import Professor
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.utils.decorators import method_decorator
from decorators import group_required
from django.contrib.auth import update_session_auth_hash

class CadastroProfessor(View):
    template = 'cadprofessor.html'

    def get(self, request):
        return render_to_response(self.template, context_instance=RequestContext(request))

    def post(self, request):
        try:
            ocorrenciaUsuario = User.objects.get(username=request.POST["login"])
            duploUser = True
            return render_to_response(self.template, {'duploUser':duploUser,'nome':request.POST["nome"],'email':request.POST["email"]}, context_instance=RequestContext(request))
            
        except ObjectDoesNotExist:

            cadprofessor = Professor(
                first_name = request.POST["nome"],username = request.POST["login"],email = request.POST["email"])
            cadprofessor.save()

            cadprofessor = User.objects.get(username=request.POST["login"])
            cadprofessor.set_password(request.POST["senha"])
            cadprofessor.groups.add(5)
            cadprofessor.save()

            confirmacao2 = True
            return render_to_response(self.template, {'confirmacao2':confirmacao2}, context_instance=RequestContext(request))
            
        return render_to_response(self.template, context_instance=RequestContext(request))
        

class PerfilProfessor(View):
    template = 'cadprofessor.html'
    @method_decorator(group_required(5))
    def get(self, request):

        return render_to_response(self.template, {'nome':request.user.first_name,
                                                'email':request.user.email, 
                                                'login':request.user.username
                                                }, context_instance=RequestContext(request))

    @method_decorator(group_required(5))
    def post(self, request):
        professor = User.objects.get(id=request.user.id)
        professor.first_name = request.POST["nome"]
        professor.email = request.POST["email"]
        professor.username = request.POST["login"]

        professor.set_password(request.POST["senha"])
        professor.save()

        confirmacao = True
        alter = True

        return render_to_response(self.template, {'nome':professor.first_name,
                                                'email':professor.email, 
                                                'login':professor.username,
                                                'alter':alter,
                                                'confirmacao':confirmacao}, context_instance=RequestContext(request))

from django.views.generic.base import View
from django.shortcuts import render
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from projeto.models.aluno import Aluno
from projeto.models.ocorrencia import Ocorrencia
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.utils.decorators import method_decorator
from decorators import group_required

class CadastroOcorrencia(View):
    template = 'cadocorrencia.html'

    @method_decorator(group_required(5))
    def get(self, request):
        alunos = Aluno.objects.all()
        return render_to_response(self.template,{'alunos':alunos}, context_instance=RequestContext(request))

    @method_decorator(group_required(5))
    def post(self, request):
        ocorrencia = Ocorrencia(
                aluno_id = request.POST["aluno"],
                professor_id = request.user.id,
                disciplina = request.POST["disciplina"],
                descricao_ocorrencia = request.POST["descricao"],
                data = request.POST["data"],
                hora = request.POST["hora"]
        )
        ocorrencia.save()
        print ocorrencia.aluno_id
        confirmacao = True
        alunos = Aluno.objects.all()
        return render_to_response(self.template,{'confirmacao':confirmacao, 'alunos':alunos}, context_instance=RequestContext(request))

class MostrarOcorrencias(View):
    template = 'ocorrencias.html'

    @method_decorator(group_required(5))
    def get(self, request):
        ocorrencias = Ocorrencia.objects.filter(professor_id = request.user.id)

        return render_to_response(self.template, {'ocorrencias':ocorrencias}, context_instance=RequestContext(request))

class OcorrenciaDetalhe(View):
    template = 'ocorrenciaDetalhe.html'    

    @method_decorator(group_required(5))
    def get(self, request, id):
        ocorrencia = Ocorrencia.objects.get(id=id)

        return render_to_response(self.template, {'ocorrencia':ocorrencia}, context_instance=RequestContext(request))

class AlterOcorrencia(View):
    template = 'alterOcorrencia.html'    

    @method_decorator(group_required(5))
    def get(self, request, id):
        ocorrencia = Ocorrencia.objects.get(id=id)

        return render_to_response(self.template,{'ocorrencia':ocorrencia}, context_instance=RequestContext(request))
    
    @method_decorator(group_required(5))
    def post(self,request, id):

        ocorrencia = Ocorrencia.objects.get(id=id)

        ocorrencia.disciplina = request.POST["disciplina"]
        ocorrencia.descricao_ocorrencia = request.POST["descricao"]

        ocorrencia.save()

        confirmacao = True
        
        return render_to_response(self.template,{'confirmacao':confirmacao, 'ocorrencia':ocorrencia}, context_instance=RequestContext(request))




        
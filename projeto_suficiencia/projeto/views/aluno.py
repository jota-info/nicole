from django.views.generic.base import View
from django.shortcuts import render
from django.shortcuts import render_to_response
from django.template import RequestContext
from projeto.models.aluno import Aluno
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.utils.decorators import method_decorator
from decorators import group_required

class CadastroAluno(View):
    template = 'cadAluno.html'

    @method_decorator(group_required(4))
    def get(self, request):
        return render_to_response(self.template, context_instance=RequestContext(request))
        #return render(request, self.template)

    @method_decorator(group_required(4))
    def post(self, request):
        try:
            ocorrenciaMatricula = Aluno.objects.get(matricula=request.POST["matricula"])
            duploMatricula = True
            return render_to_response(self.template,{'duploMatricula':duploMatricula,
                                                    'nome':request.POST["nome"],
                                                    'email':request.POST["email"],
                                                    'tel':request.POST['tel'],
                                                    'turma':request.POST['turma'],
                                                    'curso':request.POST['curso']
                                                    }, context_instance=RequestContext(request))

            # return render(request, self.template, {'duploMatricula':duploMatricula,
            #                                         'nome':request.POST["nome"],
            #                                         'email':request.POST["email"],
            #                                         'tel':request.POST['tel'],
            #                                         'turma':request.POST['turma'],
            #                                         'curso':request.POST['curso']
            #                                         })
        except ObjectDoesNotExist:
            aluno = Aluno(
                tel = request.POST["tel"],
                turma = request.POST["turma"],
                curso = request.POST["curso"],
                matricula = request.POST["matricula"],
                nome = request.POST["nome"],
                email = request.POST["email"],
                username = request.POST["matricula"]
            )
            aluno.save()

        confirmacao = True
        return render_to_response(self.template, {'confirmacao':confirmacao}, context_instance=RequestContext(request))
        #return render(request, self.template, {'confirmacao':confirmacao})

class MostrarAlunos(View):
    template = 'mostraAlunos.html'

    @method_decorator(group_required(4))
    def get(self, request):
        alunos = Aluno.objects.all()
        return render_to_response(self.template, {'alunos':alunos}, context_instance=RequestContext(request))
        #return render(request, self.template, {'alunos':alunos})

class PerfilAluno(View):
    template = 'cadAluno.html'
    @method_decorator(group_required(4))
    def get(self, request, id):
        aluno = Aluno.objects.get(id=id)
        alter = True

        return render_to_response(self.template,{   'nome':aluno.nome,
                                                    'email':aluno.email,
                                                    'tel':aluno.tel,
                                                    'turma':aluno.turma,
                                                    'curso':aluno.curso,
                                                    'alter':alter}, context_instance=RequestContext(request))

        # return render(request, self.template, {     'nome':aluno.nome,
        #                                             'email':aluno.email,
        #                                             'tel':aluno.tel,
        #                                             'turma':aluno.turma,
        #                                             'curso':aluno.curso,
        #                                             'alter':alter})

    @method_decorator(group_required(4))
    def post(self, request, id):
        aluno = Aluno.objects.get(id=id)
        aluno.tel = request.POST["tel"]
        aluno.turma = request.POST["turma"]
        aluno.curso = request.POST["curso"]
        aluno.nome = request.POST["nome"]
        aluno.email = request.POST["email"]

        aluno.save()
        confirmacao2 = True
        alter = True

        return render_to_response(self.template, {'nome':aluno.nome,
                                                    'email':aluno.email,
                                                    'tel':aluno.tel,
                                                    'turma':aluno.turma,
                                                    'curso':aluno.curso,
                                                    'alter':alter,
                                                    'confirmacao2':confirmacao2}, context_instance=RequestContext(request))
        #return render(request, self.template, {'nome':aluno.nome,
                                                    # 'email':aluno.email,
                                                    # 'tel':aluno.tel,
                                                    # 'turma':aluno.turma,
                                                    # 'curso':aluno.curso,
                                                    # 'alter':alter,
                                                    # 'confirmacao2':confirmacao2})
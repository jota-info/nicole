from django.db import models
from django.contrib.auth.models import User
from aluno import Aluno
from professor import Professor

class Ocorrencia(models.Model):
	"""docstring for Ocorrencia"""
	aluno = models.ForeignKey(Aluno)
	professor = models.ForeignKey(Professor)
	disciplina = models.CharField(max_length=200)
	descricao_ocorrencia = models.TextField()
	data = models.DateField()
	hora = models.TimeField()
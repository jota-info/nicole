# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('projeto', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Ocorrencia',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('disciplina', models.CharField(max_length=200)),
                ('descricao_ocorrencia', models.TextField()),
                ('data', models.DateField()),
                ('aluno', models.ForeignKey(to='projeto.Aluno')),
                ('professor', models.ForeignKey(to='projeto.Professor')),
            ],
        ),
    ]
